﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AdtMailer.DataTransferObject.Enums
{
    public enum SortType
    {
        [Display(Name = "По дате")]
        ByDate = 0,
        [Display(Name = "По алфавиту")]
        ByAlphabet = 1
    }
}
