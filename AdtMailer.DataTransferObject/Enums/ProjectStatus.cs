﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AdtMailer.DataTransferObject.Enums
{
    public enum ProjectStatus
    { 
        [Display(Name = "Новый")]
        New = 0,
        [Display(Name = "Ожидаем ответа")]
        Theiranswer = 1,
        [Display(Name = "Ожидают ответа")]
        Ouranswer = 2,
        [Display(Name = "В работе")]
        InProgress = 3,
        [Display(Name = "Проверка")]
        Verification = 4,
        [Display(Name = "Завершен")]
        Completed = 5,
        [Display(Name = "Отменен")]
        Canceled = 6
    }
}
