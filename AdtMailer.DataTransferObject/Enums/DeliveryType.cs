﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.DataTransferObject.Enums
{
    public enum DeliveryType
    {
        Each = 0,
        All = 1
    }
}
