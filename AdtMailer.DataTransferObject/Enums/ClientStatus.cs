﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AdtMailer.DataTransferObject.Enums
{
    public enum ClientStatus
    {
        [Display(Name = "Новый")]
        New = 0,
        [Display(Name = "Переговоры")]
        Conversation = 1,
        [Display(Name = "Сотрудничество")]
        Cooperation = 2,
        [Display(Name = "Отклонен")]
        Rejected = 3,
        [Display(Name = "Архив")]
        Archive = 4
    }
}
