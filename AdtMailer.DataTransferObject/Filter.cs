﻿using AdtMailer.DataTransferObject.Enums;
using System;

namespace AdtMailer.DataTransferObject
{
    public class Filter
    {
        public SortType SortType { get; set; }
        public string CategoryId { get; set; }
        public string CompanyId { get; set; }
        public ClientStatus? Status { get; set; }
        public int? Period { get; set; } = 7;
    }
}
