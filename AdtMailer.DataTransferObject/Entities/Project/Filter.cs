﻿using AdtMailer.DataTransferObject.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.DataTransferObject.Entities.Project
{
    public class Filter
    {
        public ProjectStatus? Status { get; set; }
    }
}
