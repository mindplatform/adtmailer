﻿using AdtMailer.Infrastructure.Models.Mail;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface IMailService
    {
        Task SendAsync(MailViewModel mail);
    }
}
