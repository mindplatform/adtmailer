﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.TasksViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class TaskMapper : IMapper<UserTask, UserTaskViewModel>
    {
        public UserTask Map(UserTaskViewModel obj)
        {
            if (obj == null) return null;
            return new UserTask()
            {
                Name = obj.Name,
                Date = obj.Date,
                Description = obj.Description,
                ApplicationUserId = obj.ApplicationUserId,
                Id = obj.Id
            };
        }

        public UserTaskViewModel Map(UserTask obj)
        {
            if (obj == null) return null;
            return new UserTaskViewModel()
            {
                Name = obj.Name,
                Date = obj.Date,
                Description = obj.Description,
                ApplicationUserId = obj.ApplicationUserId,
                Id = obj.Id
            };
        }

        public IEnumerable<UserTask> Map(IEnumerable<UserTaskViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<UserTaskViewModel> Map(IEnumerable<UserTask> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }
    }
}
