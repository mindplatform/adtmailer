﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.SettingsViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class SettingsMapper : IMapper<UserSettings, SettingsViewModel>
    {
        public UserSettings Map(SettingsViewModel obj)
        {
            if (obj == null) return null;
            return new UserSettings()
            {
                ApplicationUserId = obj.ApplicationUserId,
                EmailSettings = Newtonsoft.Json.JsonConvert.SerializeObject(obj.EmailSettings),
                Id = obj.Id,
                FirstName = obj.FirstName,
                SecondName = obj.SecondName
            };
        }

        public SettingsViewModel Map(UserSettings obj)
        {
            if (obj == null) return null;
            return new SettingsViewModel()
            {
                ApplicationUserId = obj.ApplicationUserId,
                Id = obj.Id,
                EmailSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailSettingsViewModel>(obj.EmailSettings),
                FirstName = obj.FirstName,
                SecondName = obj.SecondName
            };   
        }

        public IEnumerable<UserSettings> Map(IEnumerable<SettingsViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<SettingsViewModel> Map(IEnumerable<UserSettings> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }
    }
}
