﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.ProjectViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class ProjectMapper : IMapper<Project, ProjectViewModel>
    {
        public Project Map(ProjectViewModel obj)
        {
            if (obj == null) return null;
            return new Project()
            {
                Name = obj.Name,
                ApplicationUserId = obj.ApplicationUserId,
                CompanyId = obj.CompanyId,
                DateCreated = obj.DateCreated,
                Description = obj.Description,
                EndDate = obj.EndDate,
                StartDate = obj.StartDate,
                Id = obj.Id,
                Payment = obj.Payment,
                Prepayment = obj.Prepayment,
                Status = obj.Status,
                Files = obj.MetaFiles == null ? null : JsonConvert.SerializeObject(obj.MetaFiles)
            };
        }

        public ProjectViewModel Map(Project obj)
        {
            if (obj == null) return null;
            return new ProjectViewModel()
            {
                Name = obj.Name,
                ApplicationUserId = obj.ApplicationUserId,
                CompanyId = obj.CompanyId,
                DateCreated = obj.DateCreated,
                Description = obj.Description,
                EndDate = obj.EndDate,
                StartDate = obj.StartDate,
                Id = obj.Id,
                Payment = obj.Payment,
                Prepayment = obj.Prepayment,
                Status = obj.Status,
                MetaFiles = String.IsNullOrWhiteSpace(obj.Files) ? null : JsonConvert.DeserializeObject<List<Shared.Entities.MetaFile>>(obj.Files)
            };
        }

        public IEnumerable<Project> Map(IEnumerable<ProjectViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<ProjectViewModel> Map(IEnumerable<Project> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }
    }
}
