﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class CompanyMapper : IMapper<Company, CompanyViewModel>
    {
        private readonly IMapper<Contact, ContactViewModel> contactMapper;

        public CompanyMapper(IMapper<Contact, ContactViewModel> contactMapper)
        {
            this.contactMapper = contactMapper;
        }
        public Company Map(CompanyViewModel obj)
        {
            if (obj == null) return null;
            return new Company()
            {
                Name = obj.Name,
                DateCreated = obj.DateCreated,
                Description = obj.Description,
                Site = obj.Site,
                ApplicationUserId = obj.ApplicationUserId,
                Id = obj.Id,
                CategoryId = obj.CategoryId,
                Contacts = this.contactMapper.Map(obj.Contacts)?.ToList(),
                Status = obj.Status
            };
        }

        public CompanyViewModel Map(Company obj)
        {
            if (obj == null) return null;
            return new CompanyViewModel()
            {
                Name = obj.Name,
                DateCreated = obj.DateCreated,
                Description = obj.Description,
                Site = obj.Site,
                ApplicationUserId = obj.ApplicationUserId,
                Id = obj.Id,
                CategoryId = obj.CategoryId,
                Contacts = this.contactMapper.Map(obj.Contacts)?.ToList(),
                Status = obj.Status
            };
        }

        public IEnumerable<Company> Map(IEnumerable<CompanyViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<CompanyViewModel> Map(IEnumerable<Company> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }
    }
}
