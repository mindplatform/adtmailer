﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.FileViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class FileMapper : IMapper<File, FileViewModel>
    {
        public File Map(FileViewModel obj)
        {
            if (obj == null) return null;
            return new File()
            {
                Name = obj.Name,
                Id = obj.Id,
                Content = obj.Content,
                ContentType = obj.ContentType
            };
        }

        public FileViewModel Map(File obj)
        {
            if (obj == null) return null;
            return new FileViewModel()
            {
                Name = obj.Name,
                Id = obj.Id,
                Content = obj.Content,
                ContentType = obj.ContentType
            };
        }

        public IEnumerable<File> Map(IEnumerable<FileViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<FileViewModel> Map(IEnumerable<File> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }
    }
}
