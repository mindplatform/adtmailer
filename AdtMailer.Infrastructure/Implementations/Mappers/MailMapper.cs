﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.MailViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AdtMailer.DataTransferObject.Enums;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class MailMapper : IMapper<Mail, MailViewModel>
    {
        public Mail Map(MailViewModel obj)
        {
            if (obj == null) return null;
            return new Mail()
            {
                ApplicationUserId = obj.ApplicationUserId,
                Body = obj.Body,
                DeliveryType = obj.DeliveryType,
                Emails = obj.Emails,
                Files = this.GetFiles(obj.Files),
                From = obj.From,
                Id = obj.Id,
                Subject = obj.Subject,
                TemplateId = obj.TemplateId,
                IsRead = obj.IsRead,
                CategoryId = obj.CategoryId,
                CompanyId = obj.CompanyId,
                AddPixel = obj.AddPixel
            };
        }

        public MailViewModel Map(Mail obj)
        {
            if (obj == null) return null;
            return new MailViewModel()
            {
                ApplicationUserId = obj.ApplicationUserId,
                Body = obj.Body,
                DeliveryType = obj.DeliveryType,
                Emails = obj.Emails,
                //Files = obj.Files,
                From = obj.From,
                Id = obj.Id,
                Subject = obj.Subject,
                TemplateId = obj.TemplateId,
                IsRead = obj.IsRead
            };
        }

        public IEnumerable<Mail> Map(IEnumerable<MailViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<MailViewModel> Map(IEnumerable<Mail> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        private List<Domain.Models.File> GetFiles(IFormFileCollection files)
        {
            var filesEntity = new List<Domain.Models.File>();
            if (files != null)
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        //builder.Attachments.Add(file.FileName, file.OpenReadStream());
                        using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                        {
                            var fileEntity = new Domain.Models.File();
                            fileEntity.Name = file.Name;
                            fileEntity.Content = binaryReader.ReadBytes((int)file.Length);
                            fileEntity.ContentType = file.ContentType;
                            filesEntity.Add(fileEntity);
                        }
                    }
                }
                return filesEntity;
            }
            return null;
        }
    }
}
