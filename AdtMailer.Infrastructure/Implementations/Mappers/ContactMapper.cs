﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class ContactMapper : IMapper<Contact, ContactViewModel>
    {
        public Contact Map(ContactViewModel obj)
        {
            if (obj == null) return null;
            return new Contact()
            {
                Name = obj.Name,
                Position = obj.Position,
                Id = obj.Id,
                CompanyId = obj.CompanyId,
                ApplicationUserId = obj.ApplicationUserId,
                Emails = obj.Emails,
                Phone = obj.Phone,
                Skype = obj.Skype,
                Status = obj.Status,
                DateCreated = obj.DateCreated,
                Description = obj.Description,
                Social = obj.Social
            };
        }

        public ContactViewModel Map(Contact obj)
        {
            if (obj == null) return null;
            return new ContactViewModel()
            {
                Name = obj.Name,
                Position = obj.Position,
                Id = obj.Id,
                CompanyId = obj.CompanyId,
                ApplicationUserId = obj.ApplicationUserId,
                Emails = obj.Emails,
                Phone = obj.Phone,
                Skype = obj.Skype,
                Status = obj.Status,
                DateCreated = obj.DateCreated,
                Description = obj.Description,
                Social = obj.Social
            };
        }

        public IEnumerable<Contact> Map(IEnumerable<ContactViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<ContactViewModel> Map(IEnumerable<Contact> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }
    }
}
