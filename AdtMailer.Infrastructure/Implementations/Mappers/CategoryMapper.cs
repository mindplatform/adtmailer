﻿using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Mappers
{
    public class CategoryMapper : IMapper<Category, CategoryViewModel>
    {
        public Category Map(CategoryViewModel obj)
        {
            if (obj == null) return null;
            return new Category()
            {
                ApplicationUserId = obj.ApplicationUserId,
                Description = obj.Description,
                Name = obj.Name,
                Id = obj.Id
            };
        }

        public CategoryViewModel Map(Category obj)
        {
            if (obj == null) return null;
            return new CategoryViewModel()
            {
                ApplicationUserId = obj.ApplicationUserId,
                Description = obj.Description,
                Name = obj.Name,
                Id = obj.Id
            };
        }

        public IEnumerable<Category> Map(IEnumerable<CategoryViewModel> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }

        public IEnumerable<CategoryViewModel> Map(IEnumerable<Category> obj)
        {
            if (obj == null) return null;
            return obj.Select(x => this.Map(x));
        }
    }
}
