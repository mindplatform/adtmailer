﻿using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.SettingsViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly ISettingsRepository settingsRepository;
        private readonly IMapper<UserSettings, SettingsViewModel> settingsMapper;

        public SettingsService(ISettingsRepository settingsRepository, IMapper<UserSettings, SettingsViewModel> settingsMapper)
        {
            this.settingsRepository = settingsRepository;
            this.settingsMapper = settingsMapper;
        }
        public SettingsViewModel Find(string userId)
        {
            return this.settingsMapper.Map(this.settingsRepository.Find(userId));
        }

        public bool Update(SettingsViewModel settings)
        {
            return this.settingsRepository.Update(this.settingsMapper.Map(settings));          
        }
    }
}
