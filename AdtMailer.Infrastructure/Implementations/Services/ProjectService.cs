﻿using AdtMailer.DataTransferObject.Entities.Project;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.ProjectViewModels;
using AdtMailer.Infrastructure.Shared.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository projectRepository;
        private readonly IFileRepository fileRepository;
        private readonly IMapper<Project, ProjectViewModel> projectMapper;

        public ProjectService(IProjectRepository projectRepository, IFileRepository fileRepository, IMapper<Project, ProjectViewModel> projectMapper)
        {
            this.projectRepository = projectRepository;
            this.projectMapper = projectMapper;
            this.fileRepository = fileRepository;
        }
        public IEnumerable<ProjectViewModel> CompanyProjects(string companyId)
        {
            return this.projectMapper.Map(this.projectRepository.CompanyProjects(companyId));
        }

        public bool Delete(string projectId)
        {
            return this.projectRepository.Delete(projectId);        
        }

        public ProjectViewModel Find(string projectId)
        {
            return this.projectMapper.Map(this.projectRepository.Find(projectId));
        }

        public bool Update(ProjectViewModel project)
        {
            if (String.IsNullOrWhiteSpace(project.Id))
            {
                if (project.Files != null)
                {
                    var files = this.GetFiles(project.Files);
                    fileRepository.AddRange(files);
                    var metaFiles = new List<MetaFile>();
                    foreach (var f in files)
                    {
                        metaFiles.Add(new MetaFile() { Name = f.Name, Id = f.Id, ContentType = f.ContentType });
                    }
                    project.MetaFiles = metaFiles;
                    return this.projectRepository.Update(this.projectMapper.Map(project));
                }
            }
            if (project.Files != null)
            {
                var files = this.GetFiles(project.Files);
                fileRepository.AddRange(files);
                var metaFiles = new List<MetaFile>();
                foreach (var f in files)
                {
                    metaFiles.Add(new MetaFile() { Name = f.Name, Id = f.Id, ContentType = f.ContentType });
                }
                if (project.MetaFiles != null)
                {
                    project.MetaFiles.AddRange(metaFiles);
                }
                else
                {
                    project.MetaFiles = metaFiles;
                }
                return this.projectRepository.Update(this.projectMapper.Map(project));
            }
            return this.projectRepository.Update(this.projectMapper.Map(project));
        }

        public IEnumerable<ProjectViewModel> UserProjects(string userId, Filter filter)
        {
            return this.projectMapper.Map(this.projectRepository.UserProjects(userId, filter));
        }

        private List<Domain.Models.File> GetFiles(IFormFileCollection files)
        {
            var filesEntity = new List<Domain.Models.File>();
            if (files != null)
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        //builder.Attachments.Add(file.FileName, file.OpenReadStream());
                        using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                        {
                            var fileEntity = new Domain.Models.File();
                            fileEntity.Name = file.FileName;
                            fileEntity.Content = binaryReader.ReadBytes((int)file.Length);
                            fileEntity.ContentType = file.ContentType;
                            filesEntity.Add(fileEntity);
                            
                        }
                    }
                }
                return filesEntity;
            }
            return null;
        }
    }
}
