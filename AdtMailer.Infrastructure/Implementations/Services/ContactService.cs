﻿using AdtMailer.DataTransferObject;
using AdtMailer.DataTransferObject.Enums;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository contactRepository;
        private readonly IMapper<Contact, ContactViewModel> contactMapper;

        public ContactService(IContactRepository contactRepository, IMapper<Contact, ContactViewModel> contactMapper)
        {
            this.contactRepository = contactRepository;
            this.contactMapper = contactMapper;  
        }

        public bool Delete(string contactId)
        {
            return this.contactRepository.Delete(contactId);
        }

        public ContactViewModel Find(string contactId)
        {
            return this.contactMapper.Map(this.contactRepository.Find(contactId));
        }

        public IEnumerable<ContactViewModel> CompanyContacts(string companyId)
        {
            if (String.IsNullOrEmpty(companyId)) return null;
            return this.contactMapper.Map(this.contactRepository.CompanyContacts(companyId));
        }

        public bool Update(ContactViewModel contactViewModel)
        {
            if (contactViewModel == null)
                return false;
            return this.contactRepository.Update(this.contactMapper.Map(contactViewModel));
        }

        public IEnumerable<ContactViewModel> UserContacts(string userId, Filter filter)
        {
            return this.contactMapper.Map(this.contactRepository.UserContacts(userId, filter));
        }
    }
}
