﻿using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.FileViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class FileService : IFileService
    {
        private readonly IFileRepository fileRepository;
        private readonly IMapper<File, FileViewModel> fileMapper;

        public FileService(IFileRepository fileRepository, IMapper<File, FileViewModel> fileMapper)
        {
            this.fileRepository = fileRepository;
            this.fileMapper = fileMapper;
        }
        public FileViewModel Get(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;      
            return this.fileMapper.Map(this.fileRepository.Get(id));
        }
    }
}
