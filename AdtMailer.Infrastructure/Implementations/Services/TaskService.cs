﻿using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.SettingsViewModels;
using AdtMailer.Infrastructure.Models.TasksViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper<UserTask, UserTaskViewModel> taskMapper;
        private readonly ITaskRepository taskRepository;

        public TaskService(ITaskRepository taskRepository, IMapper<UserTask, UserTaskViewModel> taskMapper)
        {
            this.taskRepository = taskRepository;
            this.taskMapper = taskMapper;
        }
        public UserTaskViewModel Find(string id)
        {
            return this.taskMapper.Map(this.taskRepository.Find(id));
        }

        public IEnumerable<UserTaskViewModel> Get(string userId)
        {
            return this.taskMapper.Map(this.taskRepository.Get(userId));
        }

        public bool Update(UserTaskViewModel userTask)
        {
            return this.taskRepository.Update(this.taskMapper.Map(userTask));
        }
    }
}
