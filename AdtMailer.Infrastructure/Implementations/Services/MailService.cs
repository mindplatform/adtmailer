﻿using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.MailViewModels;
using System;
using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Domain.Models;
using AdtMailer.DataTransferObject.Enums;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class MailService : IMailService
    {
        private readonly ISettingsService settingsService;
        private readonly IMailRepository mailRepository;
        private readonly IMapper<Mail, MailViewModel> mailMapper;

        public MailService(ISettingsService settingsService, IMailRepository mailRepository, IMapper<Mail, MailViewModel> mailMapper)
        {
            this.settingsService = settingsService;
            this.mailRepository = mailRepository;
            this.mailMapper = mailMapper;
        }

        public bool AddClick(string mailId)
        {
            return this.mailRepository.AddClick(mailId);
        }

        public IEnumerable<MailViewModel> GetMails(string userId)
        {
            return this.mailMapper.Map(this.mailRepository.GetMails(userId));
        }

        public async Task SendAsync(MailViewModel mail, string host)
        {
            var settings = this.settingsService.Find(mail.ApplicationUserId);
            if (settings == null)
            {
                throw new ArgumentNullException();
            }
            {
                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync("smtp.yandex.ru", 465, true);
                    await client.AuthenticateAsync(settings.EmailSettings.Email, settings.EmailSettings.Password);
                  
                   var mailEntity = this.mailRepository.AddMail(this.mailMapper.Map(mail));
                    if (mail.AddPixel)
                    {
                        mail.Body += " \r\n" + $"<img src=\"{host}/message/AddClick/{ mailEntity.Id}\">";
                    }
                    var emailMessages = this.GetMimeMessages(mail);
                    if (emailMessages != null)
                        foreach (var emailMessage in emailMessages)
                        {
                            await client.SendAsync(emailMessage);
                        }
                    await client.DisconnectAsync(true);
                }
                //using (var client = new SmtpClient())
                //{

                //    client.Connect("smtp.yandex.ru", 465, true);
                //    client.Authenticate(settings.EmailSettings[0].Email, settings.EmailSettings[0].Password);
                //    var emailMessages = this.GetMimeMessages(mail);
                //    if (emailMessages != null)
                //        foreach (var emailMessage in emailMessages)
                //    {
                //        client.Send(emailMessage);
                //    }
                //    client.Disconnect(true);
                //}
            
            }
        }

        private IEnumerable<MimeMessage> GetMimeMessages(MailViewModel mail)
        {
            mail.Emails = Regex.Replace(mail.Emails, @"\s+", string.Empty);
            string[] charactersToReplace = new string[] { @"\t", @"\n", @"\r", " " };
            foreach (string s in charactersToReplace)
            {
                mail.Emails = mail.Emails.Replace(s, "");
            }
            var emails = mail.Emails.Split(',');
            if (mail.DeliveryType == DeliveryType.All)
            {
                var emailMessage = new MimeMessage();
                emailMessage.From.Add(new MailboxAddress(mail.From, "sergey.fedorov@statanly.com"));           
                foreach (var e in emails)
                {
                    if (String.IsNullOrEmpty(e))
                    {
                        continue;
                    }
                    emailMessage.To.Add(new MailboxAddress("", e));
                }
                emailMessage.Subject = mail.Subject;
                //emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html){Text = mail.Body};
                var builder = new BodyBuilder();
                // Set the plain-text version of the message text
                builder.HtmlBody = mail.Body;
                if (mail.Files != null)
                {
                    foreach (var file in mail.Files)
                    {
                        if (file.Length > 0)
                        {
                            builder.Attachments.Add(file.FileName, file.OpenReadStream());
                        }
                    }
                }
                // Now we just need to set the message body and we're done
                emailMessage.Body = builder.ToMessageBody();
                return new List<MimeMessage>() { emailMessage };          
            }
            if (mail.DeliveryType == DeliveryType.Each)
            {
                var emailMessages = new List<MimeMessage>();
                foreach (var e in emails)
                {
                    if (String.IsNullOrEmpty(e))
                    {
                        continue;
                    }
                    var emailMessage = new MimeMessage();
                    emailMessage.From.Add(new MailboxAddress(mail.From, "sergey.fedorov@statanly.com"));
                    emailMessage.To.Add(new MailboxAddress("", e));
                    emailMessage.Subject = mail.Subject;
                    //emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html){Text = mail.Body};
                    var builder = new BodyBuilder();
                    // Set the plain-text version of the message text
                    builder.HtmlBody = mail.Body;
                   if (mail.Files != null)
                    { 
                        foreach (var file in mail.Files)
                        {
                            if (file.Length > 0)
                            {
                                builder.Attachments.Add(file.FileName, file.OpenReadStream());
                            }
                        }
                    }
                    // Now we just need to set the message body and we're done
                    emailMessage.Body = builder.ToMessageBody();
                    emailMessages.Add(emailMessage);
                }
                return emailMessages;
            }
            return null;
        }
    }
}
