﻿using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly IMapper<Category, CategoryViewModel> categoryMapper;

        public CategoryService(ICategoryRepository categoryRepository, IMapper<Category, CategoryViewModel> categoryMapper)
        {
            this.categoryMapper = categoryMapper;
            this.categoryRepository = categoryRepository;
        }

        public bool Delete(string id)
        {
            return this.categoryRepository.Delete(id);
        }

        public CategoryViewModel Get(string id)
        {
            return this.categoryMapper.Map(this.categoryRepository.Get(id));
        }

        public IEnumerable<CategoryViewModel> GetCategories(string userId)
        {
            return this.categoryMapper.Map(this.categoryRepository.GetCategories(userId));
        }

        public bool Update(CategoryViewModel categoryViewModel)
        {
            return this.categoryRepository.Update(this.categoryMapper.Map(categoryViewModel));
        }
        public Dictionary<string, string> CategoryDictionary(string userId)
        {
            return this.categoryMapper.Map(this.categoryRepository.GetCategories(userId)).ToDictionary(key => key.Id, value => value.Name);
        }
    }
}
