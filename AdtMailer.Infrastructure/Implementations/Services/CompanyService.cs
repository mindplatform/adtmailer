﻿using AdtMailer.DataTransferObject;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository companyRepository;
        private readonly IMapper<Company, CompanyViewModel> companyMapper;

        public CompanyService(ICompanyRepository companyRepository, IMapper<Company, CompanyViewModel> companyMapper)
        {
            this.companyRepository = companyRepository;
            this.companyMapper = companyMapper; 
        }

        public IEnumerable<CompanyViewModel> CategoryCompanies(string categoryId)
        {
            if (String.IsNullOrEmpty(categoryId))
            {
                return null;
            }
            return this.companyMapper.Map(this.companyRepository.СategoryCompanies(categoryId));
        }

        public bool Delete(string companyId)
        {
            return this.companyRepository.Delete(companyId);
        }

        public CompanyViewModel Find(string companyId)
        {
            return this.companyMapper.Map(this.companyRepository.Find(companyId));
        }

        public bool Update(CompanyViewModel companyViewModel)
        {
            if (companyViewModel == null)
            {
                return false;
            }        
            return this.companyRepository.Update(this.companyMapper.Map(companyViewModel));
        }

        public IEnumerable<CompanyViewModel> UserCompanies(string userId, Filter filter)
        {
            return this.companyMapper.Map(this.companyRepository.UserCompanies(userId, filter));
        }
        public Dictionary<string,string> CompanyDictionary(string userId, Filter filter)
        {
            return this.companyMapper.Map(this.companyRepository.UserCompanies(userId, filter)).ToDictionary(key => key.Id, value => value.Name);
        }
    }
}
