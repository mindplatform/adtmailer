﻿using AdtMailer.Domain.Data;
using AdtMailer.Domain.Implementations.Repositories;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Implementations.Mappers;
using AdtMailer.Infrastructure.Implementations.Services;
using AdtMailer.Infrastructure.Interfaces.IMappers;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using AdtMailer.Infrastructure.Models.FileViewModels;
using AdtMailer.Infrastructure.Models.MailViewModels;
using AdtMailer.Infrastructure.Models.ProjectViewModels;
using AdtMailer.Infrastructure.Models.SettingsViewModels;
using AdtMailer.Infrastructure.Models.TasksViewModel;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace MindPlatform.Infrastructure.Implementations.Provider
{
    public static class ServiceProvider
    {
        public static void AddServices(this IServiceCollection services)
        {
            //Services 
            services.AddTransient<IMailService, MailService>();
            services.AddTransient<ISettingsService, SettingsService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<IContactService, ContactService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient <ITaskService, TaskService>();
            services.AddTransient<IFileService, FileService>();

            //Repositories 
            services.AddTransient<ISettingsRepository, SettingsRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IContactRepository, ContactRepository>();
            services.AddTransient<IMailRepository, MailRepository>();
            services.AddTransient<IProjectRepository, ProjectRepository>();
            services.AddTransient<ITaskRepository, TaskRepository>();
            services.AddTransient<IFileRepository, FileRepository>();

            //UOW
            services.AddTransient<IUnitOfWork, ApplicationDbContext>();

            //Mappers  
            services.AddTransient<IMapper<UserSettings, SettingsViewModel>, SettingsMapper>();
            services.AddTransient<IMapper<Category, CategoryViewModel>, CategoryMapper>();
            services.AddTransient<IMapper<Company, CompanyViewModel>, CompanyMapper>();
            services.AddTransient<IMapper<Contact, ContactViewModel>, ContactMapper>();
            services.AddTransient<IMapper<Mail, MailViewModel>, MailMapper>();
            services.AddTransient<IMapper<Project, ProjectViewModel>, ProjectMapper>();
            services.AddTransient<IMapper<UserTask, UserTaskViewModel>, TaskMapper>();
            services.AddTransient<IMapper<File, FileViewModel>, FileMapper>();
        }
    }
}
