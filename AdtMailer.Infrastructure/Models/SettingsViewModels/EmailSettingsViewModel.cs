﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Models.SettingsViewModels
{
    public class EmailSettingsViewModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public MailHost MailHost { get; set; }

        public bool IsDefault { get; set; }
    }
    public enum MailHost
    {
        Yandex = 0,
        Google = 1,
        Mail = 2
    }
}
