﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Models.SettingsViewModels
{
    public class SettingsViewModel
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }
        public EmailSettingsViewModel EmailSettings { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
    }

}
