﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Models.CompanyViewModels
{
    public class CategoryViewModel
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
       // public List<Company> Companies { get; set; }
    }
}
