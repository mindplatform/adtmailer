﻿using AdtMailer.DataTransferObject.Enums;
using System;

namespace AdtMailer.Infrastructure.Models.CompanyViewModels
{
    public class ContactViewModel
    {
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Emails { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string Social { get; set; }
        public string Position { get; set; }
        public DateTime DateCreated { get; set; }
        public ClientStatus Status { get; set; }
    }
}
