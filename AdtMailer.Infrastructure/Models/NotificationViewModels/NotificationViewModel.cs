﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Models.NotificationViewModels
{
    public class NotificationViewModel
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime NotificationDate { get; set; }
    }
}
