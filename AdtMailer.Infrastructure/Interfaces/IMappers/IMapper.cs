﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IMappers
{
    public interface IMapper<TSource, TDestination>
    {
        TSource Map(TDestination obj);

        TDestination Map(TSource obj);

        IEnumerable<TSource> Map(IEnumerable<TDestination> obj);

        IEnumerable<TDestination> Map(IEnumerable<TSource> obj);
    }
}
