﻿using AdtMailer.DataTransferObject.Entities.Project;
using AdtMailer.Infrastructure.Models.ProjectViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface IProjectService
    {
        IEnumerable<ProjectViewModel> CompanyProjects(string companyId);
        IEnumerable<ProjectViewModel> UserProjects(string userId, Filter filter);
        bool Update(ProjectViewModel company);
        ProjectViewModel Find(string projectId);
        bool Delete(string projectId);
    }
}
