﻿using AdtMailer.Infrastructure.Models.SettingsViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface ISettingsService
    {
        bool Update(SettingsViewModel settingsViewModel);

        SettingsViewModel Find(string userId);
    }
}
