﻿using AdtMailer.Infrastructure.Models.MailViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface IMailService
    {
        Task SendAsync(MailViewModel mail, string host);
        IEnumerable<MailViewModel> GetMails(string userId);
        bool AddClick(string mailId);
    }
}
