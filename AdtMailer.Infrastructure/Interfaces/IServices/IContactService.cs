﻿using AdtMailer.DataTransferObject;
using AdtMailer.DataTransferObject.Enums;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface IContactService
    {
        IEnumerable<ContactViewModel> CompanyContacts(string contactId);
        IEnumerable<ContactViewModel> UserContacts(string userId, Filter filter);
        bool Update(ContactViewModel contactViewModel);
        ContactViewModel Find(string contactId);
        bool Delete(string contactId);

    }
}
