﻿using AdtMailer.Infrastructure.Models.TasksViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface ITaskService
    {
        IEnumerable<UserTaskViewModel> Get(string userId);
        UserTaskViewModel Find(string id);
        bool Update(UserTaskViewModel userTask);
    }
}
