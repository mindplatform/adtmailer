﻿using AdtMailer.DataTransferObject;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface ICompanyService
    {
        IEnumerable<CompanyViewModel> CategoryCompanies(string categoryId);
        IEnumerable<CompanyViewModel> UserCompanies(string userId, Filter filter);
        bool Update(CompanyViewModel companyViewModel);
        CompanyViewModel Find(string companyId);
        bool Delete(string companyId);
        Dictionary<string, string> CompanyDictionary(string userId, Filter filter);
    }
}
