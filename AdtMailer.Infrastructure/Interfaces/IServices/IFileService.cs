﻿using AdtMailer.Infrastructure.Models.FileViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface IFileService
    {
        FileViewModel  Get(string id);
    }
}
