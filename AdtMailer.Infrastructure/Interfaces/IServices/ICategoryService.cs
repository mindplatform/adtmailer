﻿using AdtMailer.Infrastructure.Models.CompanyViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Interfaces.IServices
{
    public interface ICategoryService
    {
        IEnumerable<CategoryViewModel> GetCategories(string userId);
        bool Update(CategoryViewModel categoryViewModel);
        CategoryViewModel Get(string id);
        bool Delete(string id);
        Dictionary<string, string> CategoryDictionary(string userId);
    }
}
