﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Shared.Entities
{
    public class MetaFile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
    }
}
