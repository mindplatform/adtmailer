﻿using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.Mail;
using System;
using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

namespace AdtMailer.Infrastructure.Implementations.Services
{
    public class MailService : IMailService
    {        
        public async Task SendAsync(MailViewModel mail)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "sergey.fedorov@statanly.com"));
            emailMessage.To.Add(new MailboxAddress("", mail.Email));
            emailMessage.Subject = mail.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = mail.Body
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.yandex.ru", 25, false);
                await client.AuthenticateAsync("sergey.fedorov@statanly.com", "Statanly2018");
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }            
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "sergey.fedorov@statanly.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.yandex.ru", 25, false);
                await client.AuthenticateAsync("sergey.fedorov@statanly.com", "Statanly2018");
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}
