﻿using AdtMailer.Infrastructure.Implementations.Services;
using AdtMailer.Infrastructure.Interfaces.IServices;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace MindPlatform.Infrastructure.Implementations.Provider
{
    public static class ServiceProvider
    {
        public static void AddServices(this IServiceCollection services)
        {
            // 
            services.AddTransient<IMailService, MailService>();
        }
    }
}
