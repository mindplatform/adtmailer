﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Infrastructure.Models.Mail
{
    public class MailViewModel
    {
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
