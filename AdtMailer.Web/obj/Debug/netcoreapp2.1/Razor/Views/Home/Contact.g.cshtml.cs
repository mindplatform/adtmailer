#pragma checksum "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\Home\Contact.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6aabb74a5f4dc3851fad14eb9558e7a4cdec205e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Contact), @"mvc.1.0.view", @"/Views/Home/Contact.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Contact.cshtml", typeof(AspNetCore.Views_Home_Contact))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\_ViewImports.cshtml"
using AdtMailer.Web;

#line default
#line hidden
#line 3 "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\_ViewImports.cshtml"
using AdtMailer.Domain.Models;

#line default
#line hidden
#line 4 "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\_ViewImports.cshtml"
using AdtMailer.Web.Models.AccountViewModels;

#line default
#line hidden
#line 5 "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\_ViewImports.cshtml"
using AdtMailer.Web.Models.ManageViewModels;

#line default
#line hidden
#line 6 "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\_ViewImports.cshtml"
using AdtMailer.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6aabb74a5f4dc3851fad14eb9558e7a4cdec205e", @"/Views/Home/Contact.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"074a199cd525b07dffb60a37195e849aab58ff10", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Contact : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\HP 13-4107\Desktop\Statanly\AdtMailer\AdtMailer.Web\Views\Home\Contact.cshtml"
  
	Layout = "~/Views/Shared/_LayoutLanding.cshtml";

#line default
#line hidden
            BeginContext(60, 639, true);
            WriteLiteral(@"


<header id=""head"" class=""secondary""></header>

<!-- container -->
<div class=""container"">

	<ol class=""breadcrumb"">
		<li><a href=""index.html"">Главная</a></li>
		<li class=""active"">Контакты</li>
	</ol>

	<div class=""row"">

		<!-- Article main content -->
		<article class=""col-sm-9 maincontent"">
			<header class=""page-header"">
				<h1 class=""page-title"">Напишите нам</h1>
			</header>

			<p>
				Если у вас возникли какие-либо вопросы, пожелания и предложения, пишите нам на <a href=""mailto:hello@statanly.com"">hello@statanly.com</a> или задайте свой вопрос используя форму обратной связи:
			</p>
			<br>
			");
            EndContext();
            BeginContext(699, 862, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e4fedb269c3f437cbf9c177b2975ccbc", async() => {
                BeginContext(705, 849, true);
                WriteLiteral(@"
				<div class=""row"">
					<div class=""col-sm-4"">
						<input class=""form-control"" type=""text"" placeholder=""Имя"">
					</div>
					<div class=""col-sm-4"">
						<input class=""form-control"" type=""text"" placeholder=""Почта"">
					</div>
					<div class=""col-sm-4"">
						<input class=""form-control"" type=""text"" placeholder=""Телефон"">
					</div>
				</div>
				<br>
				<div class=""row"">
					<div class=""col-sm-12"">
						<textarea placeholder=""Ваш вопрос..."" class=""form-control"" rows=""9""></textarea>
					</div>
				</div>
				<br>
				<div class=""row"">
					<div class=""col-sm-6"">
						<label class=""checkbox""><input type=""checkbox""> Подписаться на новости сервиса</label>
					</div>
					<div class=""col-sm-6 text-right"">
						<input class=""btn btn-action"" type=""submit"" value=""Отправить"">
					</div>
				</div>
			");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1561, 1316, true);
            WriteLiteral(@"

		</article>
		<!-- /Article -->
		<!-- Sidebar -->
		<aside class=""col-sm-3 sidebar sidebar-right"">

			<div class=""widget"">
				<h4>Наш адрес:</h4>
				<address>
					Россия, Санкт-Петербург, Биржевая линия 16, ТЕХНОПАРК
				</address>
				<h4>Телефон:</h4>
				<address>
					+7-921-875-23-96
				</address>
				<h4>Почта:</h4>
				<address>
					<a href=""mailto:hello@statanly.com"">hello@statanly.com</a>
				</address>
				<h4>Компания:</h4>
				<address>
					<a href=""http://statanly.com/"">Statanly Technologies</a>
				</address>
			</div>
		</aside>
		<!-- /Sidebar -->

	</div>
</div>	<!-- /container -->

<section class=""container-full top-space"">
	<div id=""map""></div>
</section>

<!-- Social links. replace by link/instructions in template -->
<section id=""social"">
	<div class=""container"">
		<div class=""wrapper clearfix"">
			<!-- AddThis Button BEGIN -->
			<div class=""addthis_toolbox addthis_default_style"">
				<a class=""addthis_button_facebook_like"" fb:like:layo");
            WriteLiteral(@"ut=""button_count""></a>
				<a class=""addthis_button_tweet""></a>
				<a class=""addthis_button_linkedin_counter""></a>
				<a class=""addthis_button_google_plusone"" g:plusone:size=""medium""></a>
			</div>
			<!-- AddThis Button END -->
		</div>
	</div>
</section>
<!-- /social links -->
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
