﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdtMailer.Web.Models.Entities
{
    public class EntityPair
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
