﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using AdtMailer.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AdtMailer.Web.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly ICategoryService categoryService;
        private readonly ICompanyService companyService;
        private readonly UserManager<ApplicationUser> userManager;

        public CategoryController(ICategoryService categoryService, UserManager<ApplicationUser> userManager, ICompanyService companyService)
        {
            this.categoryService = categoryService;
            this.userManager = userManager;
            this.companyService = companyService;
        }
        // GET: Category
        public IActionResult Index()
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            ViewBag.Companies = this.companyService.UserCompanies(userId, null).ToList();
            var categories = this.categoryService.GetCategories(userId);
            return View(categories);
        }

        // GET: Category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(string id)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Companies = this.companyService.CategoryCompanies(id).ToList();
            var category = this.categoryService.Get(id);
            if (category == null)
            {
                RedirectToAction("Index", "Error", new { message = "Категория удалена" });
            }
            return View(category);
        }

        // POST: Category/Edit/5
        [HttpPost]   
        public ActionResult Edit(CategoryViewModel categoryViewModel)
        {
            try
            {
                categoryViewModel.ApplicationUserId = this.userManager.GetUserId(HttpContext.User);
              
                this.categoryService.Update(categoryViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(categoryViewModel);
            }
        }

        // GET: Category/Delete/5
        public IActionResult Delete(string id)
        {
            if (this.categoryService.Delete(id))
                return RedirectToAction("Index");
            return RedirectToAction("Error", new { message = "Невозможно удалить категорию, т.к она содержит компании" });
        }

        public IActionResult Error(string message)
        {
            var error = new ErrorViewModel() { Message = message };
            return View(error);
        }

    }
}