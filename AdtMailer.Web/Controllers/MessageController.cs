﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.MailViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AdtMailer.Web.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private readonly IMailService mailService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ISettingsService settingsService;
        private static readonly byte[] trackingGif =
        {
            0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x1, 0x0, 0x1, 0x0, 0x80, 0x0, 0x0, 0xff,
            0xff, 0xff, 0x0, 0x0, 0x0, 0x2c, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x0, 0x0,
            0x2, 0x2, 0x44, 0x1, 0x0, 0x3b
        };
        public MessageController(IMailService mailService, UserManager<ApplicationUser> userManager, ISettingsService settingsService)
        {
            this.mailService = mailService;
            this.userManager = userManager;
            this.settingsService = settingsService;
        }
        public IActionResult Index()
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            var emails = this.mailService.GetMails(userId);
            return View(emails);
        }
        [HttpGet]
        public IActionResult Create(MailViewModel mail)
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            var settings = this.settingsService.Find(userId);
            mail.From = settings.FirstName + " " + settings.SecondName;
            return View(mail);
        }


        [HttpPost]
        public async Task<IActionResult> Send(MailViewModel mail)
        {
            mail.ApplicationUserId = this.userManager.GetUserId(HttpContext.User);
            await this.mailService.SendAsync(mail, Request.Host.Value);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public FileResult AddClick(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }           
            this.mailService.AddClick(id);
           // asyncRunner.Run(() => mailStatisticsService.AddClick(key), AsyncRunnerOptions.BackgroundThread);

            return File(trackingGif, "image/gif");
        }
    }
}