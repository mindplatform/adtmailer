﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AdtMailer.Web.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private readonly ICategoryService categoryService;
        private readonly ICompanyService companyService;
        private readonly IContactService contactService;
        private readonly UserManager<ApplicationUser> userManager;
       

       public CompanyController(ICategoryService categoryService, UserManager<ApplicationUser> userManager, ICompanyService companyService,
           IContactService contactService)
        {
            this.categoryService = categoryService;
            this.userManager = userManager;
            this.companyService = companyService;
            this.contactService = contactService;
        }          
        public IActionResult Companies(string id)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return RedirectToAction("Index", "Category");
            }
            ViewBag.CategoryId = id;
            ViewBag.Category = this.categoryService.Get(id);
            var companies = this.companyService.CategoryCompanies(id);
            return View(companies);
        }
        [HttpGet]
        public IActionResult AddCompany(string id)
        {
            //ViewBag.CategoryId = id;
            var userId = this.userManager.GetUserId(HttpContext.User);
            ViewBag.CategoryDictionary = this.categoryService.CategoryDictionary(userId);
            return View(new CompanyViewModel() { CategoryId = id });
        }
        [HttpPost]
        public IActionResult AddCompany(CompanyViewModel companyViewModel)
        {
            companyViewModel.Id = null;
            companyViewModel.ApplicationUserId = this.userManager.GetUserId(HttpContext.User);
            this.companyService.Update(companyViewModel);
            return RedirectToAction("Companies", new { id = companyViewModel.CategoryId });
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return RedirectToAction("Index","Category");
            }            
                      
            var company = this.companyService.Find(id);
            if (company == null)
            {
                RedirectToAction("Index", "Error", new { message = "Компания удалена" });
            }
            var userId = this.userManager.GetUserId(HttpContext.User);
            ViewBag.Contacts = this.contactService.CompanyContacts(id);
            ViewBag.Category = this.categoryService.Get(company.CategoryId);
            ViewBag.CategoryDictionary = this.categoryService.CategoryDictionary(userId);
            return View(company);
        }
        [HttpPost]
        public IActionResult Edit(CompanyViewModel companyViewModel)
        { 
            this.companyService.Update(companyViewModel);
            return RedirectToAction("Companies", new { id = companyViewModel.CategoryId });
        }
        public IActionResult Delete(string id)
        {
            ViewBag.CategoryId = id;
            var company = this.companyService.Find(id);
            if (company == null)
            {
                RedirectToAction("Index", "Error", new { message = "Компания удалена" });
            }          
            if (this.companyService.Delete(id))
            {
                if (!String.IsNullOrWhiteSpace(company.CategoryId))
                {
                    return RedirectToAction("Companies", new { id = company.CategoryId});
                }
                return RedirectToAction("Companies");
            }
            return RedirectToAction("Index", "Error", new { message = "Компания не может быть удалена, т.к содержит контакты" });
        }
        public IActionResult Error(string message)
        {
            return View(message);
        }

    }
}