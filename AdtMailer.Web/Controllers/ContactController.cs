﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AdtMailer.Web.Controllers
{
    [Authorize]
    public class ContactController : Controller
    {
        private readonly IContactService contactService;
        private readonly ICompanyService companyService;
        private readonly ICategoryService categoryService;
        private readonly UserManager<ApplicationUser> userManager;
        public ContactController(IContactService contactService, UserManager<ApplicationUser> userManager,
            ICompanyService companyService, ICategoryService categoryService)
        {
            this.contactService = contactService;
            this.userManager = userManager;
            this.companyService = companyService;
            this.categoryService = categoryService;
        }        
        public IActionResult Index(string id)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return RedirectToAction("All");
            }
            ViewBag.CompanyId = id;
            var company = this.companyService.Find(id);
            ViewBag.Company = company;
            ViewBag.Category = this.categoryService.Get(company.CategoryId);
            var contacts = this.contactService.CompanyContacts(id);
            return View(contacts);
        }
        public IActionResult All()
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            ViewBag.CompanyDictionary = this.companyService.CompanyDictionary(userId, null);
            var contacts = this.contactService.UserContacts(userId, null).ToList();
            return View(contacts);
        }
        [HttpGet]
        public IActionResult AddContact(string id)
        {
            ViewBag.CompanyId = id;
            var userId = this.userManager.GetUserId(HttpContext.User);
            var companies = this.companyService.UserCompanies(userId, null).ToList();
            ViewBag.CompanyDictionary = companies.OrderBy(x => x.Name).ToDictionary(key => key.Id, value => value.Name);
            return View(new ContactViewModel() { CompanyId = id});
        }

        [HttpPost]
        public IActionResult AddContact(ContactViewModel contact)
        {
            contact.Id = null;
            contact.ApplicationUserId = this.userManager.GetUserId(HttpContext.User);
            if (this.contactService.Update(contact))
            if (String.IsNullOrWhiteSpace(contact.CompanyId))
            {
                return RedirectToAction("All");
            }
            return RedirectToAction("Index", new { id = contact.CompanyId });

        }
        [HttpGet]
        public IActionResult Update(string id)
        {        
            var contact = this.contactService.Find(id);
            if (contact == null)
            {
                RedirectToAction("Error", "Index", new { message = "Контакт удален" });
            }
            var userId = this.userManager.GetUserId(HttpContext.User);
            var companies = this.companyService.UserCompanies(userId, null).ToList();
            ViewBag.CompanyDictionary = companies.OrderBy(x => x.Name).ToDictionary(key => key.Id, value => value.Name);
            if (!String.IsNullOrWhiteSpace(contact.CompanyId))
            {
                var company = this.companyService.Find(contact.CompanyId);
                ViewBag.Company = company;
                ViewBag.Category = this.categoryService.Get(company.CategoryId);
            }
            return View(contact);
        }

        [HttpPost]
        public IActionResult Update(ContactViewModel contact)
        {
            contact.ApplicationUserId = this.userManager.GetUserId(HttpContext.User);
            if (this.contactService.Update(contact))
            {
                if (String.IsNullOrWhiteSpace(contact.CompanyId))
                {
                    return RedirectToAction("All");
                }
                return RedirectToAction("Index", new { id = contact.CompanyId });
            }
            throw new ArgumentNullException();
        }
        [HttpGet]
        public IActionResult Delete(string contactId, string companyId)
        {            
            var contact = this.contactService.Delete(contactId);
            if (String.IsNullOrWhiteSpace(companyId))
            {
                return RedirectToAction("All");
            }
            return RedirectToAction("Index", new { id = companyId });
        }
    }
}