﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace AdtMailer.Web.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index(string message)
        {
            {
                var error = new ErrorViewModel() { Message = message };
                return View(error);
            }
        }
    }
}