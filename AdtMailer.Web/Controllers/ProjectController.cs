﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models;
using AdtMailer.Infrastructure.Models.ProjectViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using AdtMailer.DataTransferObject.Entities.Project;

namespace AdtMailer.Web.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectService projectService;
        private readonly IFileService fileService;
        private readonly ICompanyService companyService;
        private readonly UserManager<ApplicationUser> userManager;

        public ProjectController(IProjectService projectService, UserManager<ApplicationUser> userManager, ICompanyService companyService, IFileService fileService)
        {
            this.projectService = projectService;
            this.userManager = userManager;
            this.companyService = companyService;
            this.fileService = fileService;
        }
        // GET: Project
        public ActionResult Index(Filter filter)
        {
            var userId = this.userManager.GetUserId(HttpContext.User);       
            var companies = this.companyService.UserCompanies(userId, null).ToList();
            ViewBag.CompanyDictionary = companies.OrderBy(x => x.Name).ToDictionary(key => key.Id, value => value.Name);
            ViewBag.Projects = this.projectService.UserProjects(userId, filter).ToList();
            return View(filter);
        }

        // GET: Project/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            var companies = this.companyService.UserCompanies(userId, null).ToList();
            ViewBag.CompanyDictionary = companies.OrderBy(x => x.Name).ToDictionary(key => key.Id, value => value.Name);
            return View();
        }

        // POST: Project/Create
        [HttpPost] 
        public ActionResult Create(ProjectViewModel project)
        {
            try
            {
                project.ApplicationUserId = this.userManager.GetUserId(HttpContext.User);
                this.projectService.Update(project);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Project/Edit/5
        public ActionResult Edit(string id)
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            var companies = this.companyService.UserCompanies(userId, null).ToList();  
            var project = this.projectService.Find(id);
            if (project == null)
            {
                return RedirectToAction("Create");
            }
            ViewBag.CompanyDictionary = companies.OrderBy(x => x.Name).ToDictionary(key => key.Id, value => value.Name);
            return View(project);
        }
        
        [HttpPost] 
        public ActionResult Edit(ProjectViewModel project)
        {
            try
            {
                this.projectService.Update(project);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        
        public ActionResult Delete(string id)
        {
            try
            {
                this.projectService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public ActionResult GetFile(string id)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(id))
                {
                    return RedirectToAction("Index", "Error", new { message = "Файла не существует или он удален" });
                }
                var file = this.fileService.Get(id);
                if (file == null)
                {
                    return RedirectToAction("Index", "Error", new { message = "Файла не существует или он удален" });
                }
                return File(file.Content, file.ContentType, file.Name);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Error", new { message = ex.Message });
            }
        }
    }
}