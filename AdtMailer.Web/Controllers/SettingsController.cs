﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.SettingsViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using AdtMailer.Domain.Models;

namespace AdtMailer.Web.Controllers
{
    public class SettingsController : Controller
    {
        private readonly ISettingsService settingsService;
        private readonly UserManager<ApplicationUser> userManager;

        public SettingsController(ISettingsService settingsService, UserManager<ApplicationUser> userManager)
        {
            this.settingsService = settingsService;
            this.userManager = userManager;
        }
        public IActionResult Index()
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            var settings = this.settingsService.Find(userId);  
            return View(settings);
        }

        [HttpPost]
        public IActionResult Update(SettingsViewModel settingsViewModel)
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            settingsViewModel.ApplicationUserId = userId;
            this.settingsService.Update(settingsViewModel);
            return View("Index");
        }
    }
}