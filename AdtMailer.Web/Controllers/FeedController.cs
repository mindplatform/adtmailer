﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.DataTransferObject;
using AdtMailer.DataTransferObject.Enums;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.CompanyViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AdtMailer.Web.Controllers
{
    [Authorize]
    public class FeedController : Controller
    {
        private readonly ICompanyService companyService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IContactService contactService;
        private readonly ICategoryService categoryService;

        public FeedController(ICompanyService companyService, UserManager<ApplicationUser> userManager, IContactService contactService, ICategoryService categoryService)
        {
            this.companyService = companyService;
            this.contactService = contactService;
            this.userManager = userManager;
            this.categoryService = categoryService;
        }
        public IActionResult Index(Filter filter)
        {
            if (!String.IsNullOrWhiteSpace(filter.CompanyId) && !String.IsNullOrWhiteSpace(filter.CategoryId))
            {
                var company = this.companyService.Find(filter.CompanyId);
                filter.CategoryId = company.CategoryId;
            }
            var userId = this.userManager.GetUserId(HttpContext.User);
            var companies = this.companyService.UserCompanies(userId, filter).ToList();
            var contacts = this.contactService.UserContacts(userId, filter).ToList();

            if ((!String.IsNullOrWhiteSpace(filter.CompanyId) || !String.IsNullOrWhiteSpace(filter.CategoryId)))
            {
                if (companies != null && companies.Any())
                {
                    var companiesId = companies.Select(y => y.Id).ToList();
                    contacts = contacts.Where(x => companiesId.Contains(x.CompanyId)).ToList();
                }
                else
                {
                    contacts = new List<ContactViewModel>();//contacts.Where(x => !String.IsNullOrWhiteSpace(x.CompanyId)).ToList();
                }
            }
            if (!String.IsNullOrWhiteSpace(filter.CompanyId))
            {
                ViewBag.Company = companies.FirstOrDefault(x => x.Id == filter.CompanyId);
            }


            ViewBag.Companies = companies;
            ViewBag.Contacts = contacts;
            ViewBag.CompanyDictionary = companies.OrderBy(x => x.Name).ToDictionary(key => key.Id, value => value.Name);
            ViewBag.CategoryDictionary = this.categoryService.CategoryDictionary(userId);

            return View(filter);
        }
        //public IActionResult GetCompanies()
        //{
        //    var userId = this.userManager.GetUserId(HttpContext.User);
        //    var companies = this.companyService.UserCompanies(userId).ToList();
        //    return PartialView(companies);
        //}
    }
}