﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdtMailer.Domain.Models;
using AdtMailer.Infrastructure.Interfaces.IServices;
using AdtMailer.Infrastructure.Models.TasksViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AdtMailer.Web.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskService taskService;
        private readonly UserManager<ApplicationUser> userManager;
        public TaskController(ITaskService taskService, UserManager<ApplicationUser> userManager)
        {
            this.taskService = taskService;
            this.userManager = userManager;
        }
        // GET: Task
        public ActionResult Index()
        {
            var userId = this.userManager.GetUserId(HttpContext.User);
            var tasks = this.taskService.Get(userId);
            return View(tasks);
        }

        // GET: Task/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Task/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Task/Create
        [HttpPost]   
        public ActionResult Create(UserTaskViewModel userTask)
        {
            try
            {
                userTask.ApplicationUserId = this.userManager.GetUserId(HttpContext.User);
                this.taskService.Update(userTask);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Task/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Task/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Task/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Task/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}