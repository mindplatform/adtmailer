﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Models
{
    public class UserSettings
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string EmailSettings { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
    }
}
