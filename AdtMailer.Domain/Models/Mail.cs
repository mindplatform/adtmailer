﻿using AdtMailer.DataTransferObject.Enums;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Models
{
    public class Mail
    {
        public string Id { get; set; }
        public string TemplateId { get; set; }
        public string ApplicationUserId { get; set; }
        public string Emails { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }       
        public List<File> Files { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public bool IsRead { get; set; }        
        public bool AddPixel { get; set; }
        public string CompanyId { get; set; }
        public string CategoryId { get; set; }

    }
}
