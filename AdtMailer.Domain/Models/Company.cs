﻿using AdtMailer.DataTransferObject.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Models
{
    public class Company
    {
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Emails { get; set; }
        public string Site { get; set; }
        public DateTime DateCreated { get; set; }
        public List<Contact> Contacts { get; set; }
        public ClientStatus Status { get; set; }
    }
}
