﻿using AdtMailer.DataTransferObject.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Models
{
    public class Contact
    {
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Emails { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string Social { get; set; }
        public string Description { get; set; }
        public string Position { get; set; }
        public DateTime DateCreated { get; set; }
        public ClientStatus Status { get; set; }

    }
}
