﻿using AdtMailer.DataTransferObject.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Models
{
    public class Project
    {
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public ProjectStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Payment { get; set; }
        public double Prepayment { get; set; }
        public string Files { get; set; }
    }
}
