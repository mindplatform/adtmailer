﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Models
{
    public class Category
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        //public List<Company> Companies { get; set; }
    }
}
