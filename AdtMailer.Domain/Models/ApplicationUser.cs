﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace AdtMailer.Domain.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string CompanyId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string CompanyName { get; set; }

        public List<Category> Categories { get; set; }
    }
}
