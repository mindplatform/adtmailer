﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdtMailer.Domain.Data.Migrations
{
    public partial class Mgr13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Contacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Skype",
                table: "Contacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Site",
                table: "Companies",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "Skype",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "Site",
                table: "Companies");
        }
    }
}
