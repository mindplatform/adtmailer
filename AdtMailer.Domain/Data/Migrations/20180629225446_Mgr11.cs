﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdtMailer.Domain.Data.Migrations
{
    public partial class Mgr11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsRead",
                table: "Mails",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRead",
                table: "Mails");
        }
    }
}
