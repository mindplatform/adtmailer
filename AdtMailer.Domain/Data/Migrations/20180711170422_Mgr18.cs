﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdtMailer.Domain.Data.Migrations
{
    public partial class Mgr18 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Files",
                table: "Projects",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Files",
                table: "Projects");
        }
    }
}
