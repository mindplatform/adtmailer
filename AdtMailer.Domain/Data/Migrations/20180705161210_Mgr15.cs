﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdtMailer.Domain.Data.Migrations
{
    public partial class Mgr15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "UserSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondName",
                table: "UserSettings",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AddPixel",
                table: "Mails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "CategoryId",
                table: "Mails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyId",
                table: "Mails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "UserSettings");

            migrationBuilder.DropColumn(
                name: "SecondName",
                table: "UserSettings");

            migrationBuilder.DropColumn(
                name: "AddPixel",
                table: "Mails");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Mails");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Mails");
        }
    }
}
