﻿using AdtMailer.Domain.Data;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class MailRepository : IMailRepository
    {
        private readonly ApplicationDbContext dataContext;

        public MailRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }
        public Mail Find(string mailId)
        {
            if (String.IsNullOrEmpty(mailId))
            {
                return null;
            }
            return this.dataContext.Mails.Find(mailId);        
        }

        public bool AddClick(string mailId)
        {
            if (String.IsNullOrEmpty(mailId))
            {
                return false;
            }
            var mail = this.dataContext.Mails.Find(mailId);
            if (mail != null)
            {
                mail.IsRead = true;
                return this.dataContext.Save();
            }
            return false;
        }

        public Mail AddMail(Mail mail)
        {
            if (mail == null)
            {
                return null;
            }
            this.dataContext.Mails.Add(mail);
            this.dataContext.Save();
            return mail;
        }

        public IEnumerable<Mail> GetMails(string userId)
        {
            if (String.IsNullOrEmpty(userId))
            {
                return null;
            }
            return this.dataContext.Mails.Where(x => x.ApplicationUserId == userId);         
        }
    }
}
