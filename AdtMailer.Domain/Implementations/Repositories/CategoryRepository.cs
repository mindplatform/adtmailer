﻿using AdtMailer.Domain.Data;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext dataContext;

        public CategoryRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }

        public Category Get(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            return this.dataContext.Categories.Find(id);
        }

        public bool Delete(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return false;
            }
            var category = this.Get(id);
            if (category != null)
            {
                var companies = this.dataContext.Companies.Where(x => x.CategoryId == id).ToList();
                if (companies.Any())
                {
                    return false;
                }
                this.dataContext.Categories.Remove(category);
                return this.dataContext.Save();
            }
            return false;
        }
        public IEnumerable<Category> GetCategories(string userId)
        {
            if (String.IsNullOrEmpty(userId))
            {
                return null;
            }
            return this.dataContext.Categories.Where(x => x.ApplicationUserId == userId).OrderBy(x => x.Name);
        }

        public bool Update(Category category)
        {
            var categoryEntity = this.dataContext.Categories.FirstOrDefault(x => x.Id == category.Id);
            if (categoryEntity != null)
            {
                categoryEntity.Description = category.Description;
                categoryEntity.Name = category.Name;
                return dataContext.Save();
            }            
            this.dataContext.Categories.Add(category);
            return dataContext.Save();
        }
    }
}
