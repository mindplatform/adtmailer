﻿using AdtMailer.Domain.Data;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly ApplicationDbContext dataContext;

        public TaskRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }
        public UserTask Find(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            return this.dataContext.Tasks.Find(id);
        }

        public IEnumerable<UserTask> Get(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                return null;
            }
            return this.dataContext.Tasks.Where(x => x.ApplicationUserId == userId);
        }

        public bool Update(UserTask userTask)
        {
            if (userTask == null)
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(userTask.Id))
            {
                this.dataContext.Tasks.Add(userTask);
                return this.dataContext.Save();
            }

            var taskEntity = this.dataContext.Tasks.Find(userTask.Id);
            if (taskEntity != null)
            {
                taskEntity.Name = userTask.Name;
                taskEntity.Description = userTask.Description;
                taskEntity.Date = userTask.Date;
                return this.dataContext.Save();                     
            }
            if (taskEntity == null)
            {
                this.dataContext.Tasks.Add(userTask);
                return this.dataContext.Save();
            }
            return false;
        }
    }
}
