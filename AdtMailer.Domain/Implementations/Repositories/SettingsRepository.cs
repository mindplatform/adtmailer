﻿using AdtMailer.Domain.Data;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class SettingsRepository : ISettingsRepository
    {
        private readonly ApplicationDbContext dataContext;

        public SettingsRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }

        public UserSettings Find(string userId)
        {
            var settings = this.dataContext.UserSettings.FirstOrDefault(x => x.ApplicationUserId == userId);
            return settings;
        }

        public bool Update(UserSettings userSettings)
        {
            var settingsEntity = this.dataContext.UserSettings.Find(userSettings.Id);
            if (settingsEntity != null)
            {
                settingsEntity.EmailSettings = userSettings.EmailSettings;
                settingsEntity.FirstName = userSettings.FirstName;
                settingsEntity.SecondName = userSettings.SecondName;
                return dataContext.Save();
            }
            this.dataContext.UserSettings.Add(userSettings);
            return dataContext.Save();
        }
    }
}
