﻿using AdtMailer.DataTransferObject;
using AdtMailer.Domain.Data;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly ApplicationDbContext dataContext;

        public CompanyRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }
        public IEnumerable<Company> СategoryCompanies(string categoryId)
        {
            if (String.IsNullOrWhiteSpace(categoryId))
            {
                return null;
            }
            return this.dataContext.Companies.Where(x => x.CategoryId == categoryId);
        }

        public bool Update(Company company)
        {
            var companyEntity = this.dataContext.Companies.FirstOrDefault(x => x.Id == company.Id);
            if (companyEntity != null)
            {
                companyEntity.Name = company.Name;
                companyEntity.Description = company.Description;
                companyEntity.Site = company.Site;
                companyEntity.CategoryId = company.CategoryId;
                companyEntity.Status = company.Status;
                return dataContext.Save();
            }
            company.DateCreated = DateTime.Now;
            this.dataContext.Companies.Add(company);
            return dataContext.Save();
        }

        public IEnumerable<Company> UserCompanies(string userId, Filter filter)
        {
            if (String.IsNullOrWhiteSpace(userId))
            {
                return null;
            }
            if (filter == null)
            {
                return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId).OrderBy(x => x.Name);
            }
            if (!filter.Period.HasValue)
            {
                if (filter.SortType == DataTransferObject.Enums.SortType.ByAlphabet)
                {
                    if (filter.Status != null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status).OrderBy(x => x.Name);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId && x.Status == filter.Status).OrderBy(x => x.Name);
                        }
                    }
                    if (filter.Status == null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId).OrderBy(x => x.Name);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId).OrderBy(x => x.Name);
                        }
                    }

                }
                if (filter.SortType == DataTransferObject.Enums.SortType.ByDate)
                {
                    if (filter.Status != null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status).OrderByDescending(x => x.DateCreated);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId && x.Status == filter.Status).OrderByDescending(x => x.DateCreated);
                        }
                    }
                    if (filter.Status == null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId).OrderByDescending(x => x.DateCreated);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId).OrderByDescending(x => x.DateCreated);
                        }
                    }

                }
            }
            else
            {
                if (filter.SortType == DataTransferObject.Enums.SortType.ByAlphabet)
                {
                    if (filter.Status != null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId && x.Status == filter.Status && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name);
                        }
                    }
                    if (filter.Status == null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name);
                        }
                    }

                }
                if (filter.SortType == DataTransferObject.Enums.SortType.ByDate)
                {
                    if (filter.Status != null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId && x.Status == filter.Status && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated);
                        }
                    }
                    if (filter.Status == null)
                    {
                        if (String.IsNullOrWhiteSpace(filter.CategoryId))
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated);
                        }
                        else
                        {
                            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId && x.CategoryId == filter.CategoryId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated);
                        }
                    }

                }
            }
            return this.dataContext.Companies.Where(x => x.ApplicationUserId == userId);
        }

        public Company Find(string companyId)
        {
            if (String.IsNullOrWhiteSpace(companyId))
            {
                return null;
            }
            return this.dataContext.Companies.Find(companyId);
        }

        public bool Delete(string companyId)
        {
            if (String.IsNullOrWhiteSpace(companyId))
            {
                return false;
            }
            var company = this.dataContext.Companies.Find(companyId);
            if (company != null)
            {
                this.dataContext.Companies.Remove(company);
                return this.dataContext.Save();
            }
            return false;
        }
    }
}
