﻿using AdtMailer.DataTransferObject.Entities.Project;
using AdtMailer.Domain.Data;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ApplicationDbContext dataContext;

        public ProjectRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }
        public bool Update(Project project)
        {
            var projectEntity = this.dataContext.Projects.Find(project.Id);
            if (projectEntity != null)
            {
                projectEntity.Name = project.Name;
                projectEntity.Payment = project.Payment;
                projectEntity.Prepayment = project.Prepayment;
                projectEntity.StartDate = project.StartDate;
                projectEntity.Status = project.Status;
                projectEntity.Description = project.Description;
                projectEntity.EndDate = project.EndDate;
                projectEntity.CompanyId = project.CompanyId;
                projectEntity.Files = project.Files;
                return this.dataContext.Save();
            }
            project.DateCreated = DateTime.Now;
            this.dataContext.Projects.Add(project);
            return this.dataContext.Save();
        }

        public IEnumerable<Project> UserProjects(string userId, Filter filter)
        {
            if (String.IsNullOrWhiteSpace(userId))
            {
                return null;
            }
            if (filter == null || filter.Status == null)
            {
                return this.dataContext.Projects.Where(x => x.ApplicationUserId == userId).OrderBy(x=> x.Name);
            }
            if (filter != null && filter.Status != null)
            {
                return this.dataContext.Projects.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status).OrderBy(x => x.Name);
            }
            return this.dataContext.Projects.Where(x => x.ApplicationUserId == userId).OrderBy(x => x.Name);
        }

        public IEnumerable<Project> CompanyProjects(string companyId)
        {
            if (String.IsNullOrWhiteSpace(companyId))
            {
                return null;
            }
            return this.dataContext.Projects.Where(x => x.CompanyId == companyId);
        }

        public Project Find(string projectId)
        {
            if (String.IsNullOrWhiteSpace(projectId))
            {
                return null;
            }
            return this.dataContext.Projects.Find(projectId);
        }

        public bool Delete(string projectId)
        {
            if (String.IsNullOrWhiteSpace(projectId))
            {
                return false;
            }
            var project = this.dataContext.Projects.Find(projectId);
            if (project != null)
            {
                this.dataContext.Projects.Remove(project);
                return this.dataContext.Save();
            }
            return false;
        }
    }
}
