﻿using AdtMailer.Domain.Data;
using AdtMailer.DataTransferObject.Enums;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdtMailer.DataTransferObject;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class ContactRepository : IContactRepository
    {
        private readonly ApplicationDbContext dataContext;

        public ContactRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }

        public bool Delete(string contactId)
        {
            if (String.IsNullOrEmpty(contactId))
            {
                return false;
            }
            var contact = this.dataContext.Contacts.Find(contactId);
            this.dataContext.Contacts.Remove(contact);
            return dataContext.Save(); 
        }

        public Contact Find(string contactId)
        {
            if (String.IsNullOrEmpty(contactId))
            {
                return null;
            }
            return this.dataContext.Contacts.Find(contactId);
        }

        public IEnumerable<Contact> CompanyContacts(string companyId)
        {
            if (String.IsNullOrEmpty(companyId))
            {
                return null;
            }
            return this.dataContext.Contacts.Where(x => x.CompanyId == companyId);
        }

        public bool Update(Contact contact)
        {
            var contactEntity = this.dataContext.Contacts.FirstOrDefault(x => x.Id == contact.Id);
            if (contactEntity != null)
            {
                contactEntity.DateCreated = DateTime.Now;
                contactEntity.Emails = contact.Emails;
                contactEntity.Name = contact.Name;
                contactEntity.Status = contact.Status;
                contactEntity.Position = contact.Position;
                contactEntity.ApplicationUserId = contact.ApplicationUserId;
                contactEntity.Description = contact.Description;
                contactEntity.Phone = contact.Phone;
                contactEntity.Skype = contact.Skype;
                contactEntity.Social = contact.Social;
                contactEntity.CompanyId = contact.CompanyId;
                return dataContext.Save();
            }
            contact.DateCreated = DateTime.Now;
            this.dataContext.Contacts.Add(contact);
            return dataContext.Save();
        }

        public IEnumerable<Contact> UserContacts(string userId, Filter filter)
        {
            if (String.IsNullOrEmpty(userId))
            {
                return null;
            }
            if (filter == null)
            {
                return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId).OrderBy(x => x.Name);
            }
            if (!filter.Period.HasValue)
            {
                if (filter.SortType == SortType.ByAlphabet)
                {
                    if (String.IsNullOrWhiteSpace(filter.CompanyId))
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status).OrderBy(x => x.Name); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId).OrderBy(x => x.Name); }

                    }
                    else
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.CompanyId == filter.CompanyId).OrderBy(x => x.Name); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.CompanyId == filter.CompanyId).OrderBy(x => x.Name); }

                    }
                }
                if (filter.SortType == SortType.ByDate)
                {
                    if (String.IsNullOrWhiteSpace(filter.CompanyId))
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status).OrderByDescending(x => x.DateCreated); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId).OrderByDescending(x => x.DateCreated); }
                    }
                    else
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.CompanyId == filter.CompanyId).OrderByDescending(x => x.DateCreated); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.CompanyId == filter.CompanyId).OrderByDescending(x => x.DateCreated); }
                    }
                }
            }
            else
            {
                if (filter.SortType == SortType.ByAlphabet)
                {
                    if (String.IsNullOrWhiteSpace(filter.CompanyId))
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name); }

                    }
                    else
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.CompanyId == filter.CompanyId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.CompanyId == filter.CompanyId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderBy(x => x.Name); }

                    }
                }
                if (filter.SortType == SortType.ByDate)
                {
                    if (String.IsNullOrWhiteSpace(filter.CompanyId))
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated); }
                    }
                    else
                    {
                        if (filter.Status != null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.Status == filter.Status && x.CompanyId == filter.CompanyId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated); }
                        if (filter.Status == null)
                        { return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId && x.CompanyId == filter.CompanyId && x.DateCreated > DateTime.Now.AddDays(-(double)filter.Period)).OrderByDescending(x => x.DateCreated); }
                    }
                }
            }
            return this.dataContext.Contacts.Where(x => x.ApplicationUserId == userId);
        }  
    }
}
