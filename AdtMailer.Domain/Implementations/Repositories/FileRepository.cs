﻿using AdtMailer.Domain.Data;
using AdtMailer.Domain.Interfaces.IRepositories;
using AdtMailer.Domain.Interfaces.IUOW;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Implementations.Repositories
{
    public class FileRepository : IFileRepository
    {
        private readonly ApplicationDbContext dataContext;

        public FileRepository(IUnitOfWork unitOfWork)
        {
            dataContext = unitOfWork as ApplicationDbContext;
        }
        public File Add(File file)
        {
            this.dataContext.Files.Add(file);
            this.dataContext.Save();
            return file;
        }

        public IEnumerable<File> AddRange(IEnumerable<File> files)
        {
            this.dataContext.Files.AddRange(files);
            this.dataContext.Save();
            return files;
        }

        public File Get(string id)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            return this.dataContext.Files.Find(id);
        }
    }
}
