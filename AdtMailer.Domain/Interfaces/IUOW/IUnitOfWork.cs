﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IUOW
{
    public interface IUnitOfWork : IDisposable
    {
        bool Save();
    }
}
