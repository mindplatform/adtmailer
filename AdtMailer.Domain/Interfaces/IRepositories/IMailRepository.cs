﻿using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface IMailRepository
    {
        IEnumerable<Mail> GetMails(string userId);
        Mail AddMail(Mail mail);
        bool AddClick(string mailId);
        Mail Find(string mailId);
    }
}
