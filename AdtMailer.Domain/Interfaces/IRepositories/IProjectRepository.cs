﻿using AdtMailer.DataTransferObject.Entities.Project;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface IProjectRepository
    {
        IEnumerable<Project> CompanyProjects(string companyId);
        IEnumerable<Project> UserProjects(string userId, Filter filter);
        bool Update(Project company);
        Project Find(string projectId);
        bool Delete(string projectId);
    }
}
