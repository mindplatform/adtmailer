﻿using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface ISettingsRepository
    {
        bool Update(UserSettings userSettings);

        UserSettings Find(string userId);
    }
}
