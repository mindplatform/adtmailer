﻿using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface ITaskRepository
    {
        IEnumerable<UserTask> Get(string userId);
        UserTask Find(string id);
        bool Update(UserTask userTask);
    }
}
