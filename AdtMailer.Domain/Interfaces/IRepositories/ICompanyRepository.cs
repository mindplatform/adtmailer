﻿using AdtMailer.DataTransferObject;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface ICompanyRepository
    {
        IEnumerable<Company> СategoryCompanies(string categoryId);
        IEnumerable<Company> UserCompanies(string userId, Filter filter);
        bool Update(Company company);
        Company Find(string companyId);
        bool Delete(string companyId);
    }
}
