﻿using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface IFileRepository
    {
        File Get(string id);
        File Add(File file);
        IEnumerable<File> AddRange(IEnumerable<File> files);
    }
}
