﻿using AdtMailer.DataTransferObject;
using AdtMailer.DataTransferObject.Enums;
using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface IContactRepository
    {
        IEnumerable<Contact> CompanyContacts(string companyId);
        IEnumerable<Contact> UserContacts(string userId, Filter filter);
        bool Update(Contact contact);
        Contact Find(string contactId);
        bool Delete(string contactId);
    }
}
