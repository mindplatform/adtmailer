﻿using AdtMailer.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdtMailer.Domain.Interfaces.IRepositories
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetCategories(string userId);
        bool Update(Category category);
        Category Get(string id);
        bool Delete(string id);
    }
}
